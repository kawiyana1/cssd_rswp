﻿using CSSD.Entities.SIM;
using CSSD.Helper;
using CSSD.Models.Tools;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CSSD.Controllers.Tools
{
    public class CancelPostingController : Controller
    {
        // GET: CancelPosting
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public string List(DateTime? start, DateTime? end)
        {
            try
            {

                using (var s = new SIMEntities())
                {
                    var proses = s.CSSD_ListCancelPosting.AsQueryable();
                    if (start != null) proses = proses.Where(x => x.Tgl_Penerimaan >= start.Value);
                    if (end != null) proses = proses.Where(x => x.Tgl_Penerimaan <= end.Value);
                    var models = proses.ToList();
                    var r = models.ConvertAll(m => new
                    {
                        NoTransaksi = m.No_Penerimaan,
                        Tanggal = m.Tgl_Penerimaan == null ? "" : m.Tgl_Penerimaan.ToString("dd/MM/yyyy"),
                        Section = m.SectionNAme,
                        Nilai = m.Total_Nilai,
                        Keterangan = m.Keterangan
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string Save(List<CancelPostingModel> model)
        {
            
            using (var s = new SIMEntities())
            {
                //using (var dbContextTransaction = s.Database.BeginTransaction())
                //{
                try
                {
                    if (model == null) model = new List<CancelPostingModel>();
                    foreach (var x in model)
                        s.CSSD_InsertCancelPosting(x.No_Faktur);

                    s.SaveChanges();
                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $"CancelPosting-PROSES;".ToLower()
                    };
                    UserActivity.InsertUserActivity(userActivity);
                    //dbContextTransaction.Commit();

                    return JsonConvert.SerializeObject(new { IsSuccess = true });
                }
                catch (SqlException ex)
                {
                    //dbContextTransaction.Rollback(); 
                    return HConvert.Error(ex);
                }
                catch (Exception ex)
                {
                    //dbContextTransaction.Rollback(); 
                    return HConvert.Error(ex);
                }
                //}
            }
        }

    }
}