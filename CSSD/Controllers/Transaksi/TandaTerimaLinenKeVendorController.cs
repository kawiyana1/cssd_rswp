﻿using CSSD.Entities.SIM;
using CSSD.Helper;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;
using System.Linq.Dynamic;
using CSSD.Models.Transaksi;
using System.Security.Cryptography.X509Certificates;
using System.Web.ModelBinding;
using System.Xml;

namespace CSSD.Controllers.Transaksi
{
    public class TandaTerimaLinenKeVendorController : Controller
    {
        // GET: TandaTerimaLinenKeVendor
        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.CSSD_ListTandaTerima.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "PeriodeStart" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tanggal >= d);
                        }
                        else if (x.Key == "PeriodeEnd" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tanggal <= d);
                        }
                        else if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                                y.No_TT.Contains(x.Value) ||
                                y.No_SuratJalan.Contains(x.Value) ||
                                y.Supplier_Kode_Supplier.Contains(x.Value) ||
                                y.Supplier_Nama_Supplier.Contains(x.Value) ||
                                y.Keterangan.Contains(x.Value)
                            );

                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "DESC" : "ASC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        No_TT = x.No_TT,
                        Tanggal = x.Tanggal.ToString("dd/MM/yyyy"),
                        No_SuratJalan = x.No_SuratJalan,
                        Supplier_Kode_Supplier = x.Supplier_Kode_Supplier,
                        Supplier_Nama_Supplier = x.Supplier_Nama_Supplier,
                        Keterangan = x.Keterangan ?? "",
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, TandaTerimaLinenKeVendorModel model)
        {
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        string id = "";
                        if (_process == "CREATE")
                        {
                            if (model.Detail == null) model.Detail = new List<TandaTerimaLinenKeVendorDetailModel>();
                            if (model.Detail.Count == 0) throw new Exception("Detail tidak boleh kosong");
                            id = s.CSSD_InsertHeaderTandaTerima(
                                model.Tanggal,
                                model.Kode_Supplier,
                                model.Keterangan
                            ).FirstOrDefault();

                            foreach (var x in model.Detail)
                            {
                                s.CSSD_InsertDetailTandaTerima(
                                    id,
                                    x.Kode_Item,
                                    x.JenisJasa,
                                    x.Qty,
                                    x.Keterangan
                                );
                            }
                            s.SaveChanges();
                        }
                        else if (_process == "DELETE")
                        {
                            
                            s.CSSD_DeleteTandaTerima(model.NoTandaTerima);
                            s.SaveChanges();
                        }



                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"TandaTerimaLinenKeVendor-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new { IsSuccess = true, Id = id });
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
        }

        [HttpPost]
        public string Detail(string id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.CSSD_GetHeaderTandaTerima.FirstOrDefault(x => x.NoTandaTerima == id);

                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d = s.CSSD_GetDetailTandaTerima.Where(x => x.NoTandaTerima == m.NoTandaTerima).OrderByDescending(x => x.Qty).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            NoTandaTerima = m.NoTandaTerima,
                            Tanggal = m.Tanggal.ToString("yyyy-MM-dd"),
                            Kode_Supplier = m.KOde_Supplier,
                            Nama_Supplier = m.Nama_Supplier,
                            Keterangan = m.Keterangan
                        },
                        Detail = d.ConvertAll(x => new
                        {
                            Kode_Barang = x.Kode_Item,
                            Nama_Barang = x.Nama_Barang,
                            Kode_Satuan = x.Satuan_Stok,
                            JenisJasa = x.JenisJasa,
                            Qty = x.Qty,
                            Keterangan = x.Keterangan
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P

        [HttpPost]
        public string ListLookupPasien(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {

                    var proses = s.CSSD_GetDataLaundryLinenKotorInternal.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                                y.Kode_Barang.Contains(x.Value) ||
                                y.Nama_Barang.Contains(x.Value));
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        Kode_Barang = x.Kode_Barang,
                        Nama_Barang = x.Nama_Barang,
                        Kode_Satuan = x.Kode_Satuan_Beli,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P - Supplier

        [HttpPost]
        public string ListLookupSupplier(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.CSSD_ListVendorTandaTerima.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                                y.Kode_Supplier.Contains(x.Value) ||
                                y.Nama_Supplier.Contains(x.Value));
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        Kode_Supplier = x.Kode_Supplier,
                        Nama_Supplier = x.Nama_Supplier,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

    }
}