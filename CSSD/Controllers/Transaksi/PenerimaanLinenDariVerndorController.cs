﻿using CSSD.Entities.SIM;
using CSSD.Helper;
using CSSD.Models.Transaksi;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace CSSD.Controllers.Transaksi
{
    public class PenerimaanLinenDariVerndorController : Controller
    {
        // GET: PenerimaanLinenDariVerndor
        #region ===== L I S T
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
          
            var status = filter.FirstOrDefault(x => x.Key == "Realisasi").Value == "Realisasi" ? "Realisasi" : "Open";
            if (status == "Realisasi")
                return _listrealisasi(orderby, orderbytype, pagesize, pageindex, filter);
            else
                return _listopen(orderby, orderbytype, pagesize, pageindex, filter);
        }

        public string _listopen(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
               
                int totalcount;
                orderby = orderby == "No_DO" ? "" :
                    orderby == "Supplier_Nama_Supplier" ? "Nama_Supplier" :
                    orderby == "Tgl_Penerimaan" ? "Tgl_Order" :
                    orderby == "No_Penerimaan" ? "No_Order" :
                    "";

                using (var s = new SIMEntities())
                {
                    var proses = s.CSSD_ListOpenPenerimaanLinen.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.No_Order.Contains(x.Value) || 
                                y.Supplier_Nama_Supplier.Contains(x.Value) ||
                                y.No_Order.Contains(x.Value));
                        }
                        else if (x.Key == "PeriodeStart" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tgl_Order >= d);
                        }
                        else if (x.Key == "PeriodeEnd" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tgl_Order <= d);
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Tanggal = m.Tgl_Order.ToString("yyyy-MM-dd"),
                        No = m.No_Order,
                        NoDO = "",
                        Realisasi = false,
                        OrderId = m.Order_ID,
                        Supplier = m.Supplier_Nama_Supplier
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        public string _listrealisasi(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
           

                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.CSSD_ListRealisasiPenerimaanLinen.AsQueryable().Where(x=> x.Status_Batal == false);
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.No_DO.Contains(x.Value) ||
                                y.Nama_Supplier.Contains(x.Value) ||
                                y.No_Penerimaan.Contains(x.Value));
                        }
                        else if (x.Key == "PeriodeStart" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tgl_Penerimaan >= d);
                        }
                        else if (x.Key == "PeriodeEnd" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tgl_Penerimaan <= d);
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Tanggal = m.Tgl_Penerimaan.ToString("yyyy-MM-dd"),
                        No = m.No_Penerimaan,
                        NoDO = m.No_DO,
                        Realisasi = true,
                        Supplier = m.Nama_Supplier
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, PenerimaanLinenDariVerndorModel model)
        {
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        decimal id = model.No_Penerimaan;
                        if (_process == "CREATE")
                        {
                            if (model.Detail == null) model.Detail = new List<PenerimaanLinenDariVerndordetailModel>();
                            if (model.Detail.Count == 0) throw new Exception("Detail tidak boleh kosong");
                            //lokasiid = Response.Cookies["CSSD_Section_LokasiId"].Value;
                            id = s.CSSD_InsertHeaderPenerimaanLinen(
                                model.Tanggal,
                                model.Keterangan,
                                model.No_DO,
                                model.Kode_Supplier,
                                model.No_TT.ToString(), 
                                model.Total_Nilai,
                                model.Lokasi_ID,
                                model.Tgl_JatuhTempo,
                                model.Potongan
                            ).FirstOrDefault() ?? 0;

                            
                            foreach (var x in model.Detail)
                            {
                                var jum = (decimal)x.Qty_Penerimaan * x.Harga_Beli;
                                var diskon =  jum / 100 * (decimal)x.Diskon_1;
                                s.CSSD_InsertDetailPenerimaanLinen(
                                    x.Harga_Beli,
                                    x.Diskon_1,
                                    x.Qty_Penerimaan,
                                    (int)id,
                                    x.Barang_ID,
                                    diskon,
                                    model.Lokasi_ID
                                );
                            }
                            s.SaveChanges();
                        }
                        else if (_process == "DELETE")
                        {
                            foreach (var x in model.Detail)
                            {
                                var barang = s.CSSD_GetDetailPenerimaanLinen.FirstOrDefault(z => z.Penerimaan_ID == x.Penerimaan_ID);
                                s.CSSD_DeletePenerimaanLinen(barang.Penerimaan_ID, x.Barang_ID);
                            }
                            s.SaveChanges();
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"Penerimaan-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new { IsSuccess = true, Id = id });
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
        }

        [HttpPost]
        public string DetailOrder(string id)
        {

            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.CSSD_GetNoPOPenerimaanLinen.FirstOrDefault(x => x.No_Order == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d = s.CSSD_GetNoPOPenerimaanLinen_Detail.Where(x => x.No_Order == m.No_Order).OrderByDescending(x => x.Qty).ToList();

                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {

                            Tgl_Penerimaan = m.Tgl_Order.ToString("yyyy-MM-dd"),
                            OrderId = m.Order_ID,
                            Kode_Supplier = m.Supplier_Kode_Supplier,
                            Nama_Supplier = m.Supplier_Nama_Supplier, 
                            No_Order = m.No_Order,
                        },
                        Detail = d.ConvertAll(x => new
                        {
                            Id = x.Barang_ID,
                            Kode_Barang = x.Kode_Barang,
                            Nama_Barang = x.Nama_Barang,
                            Satuan_Stok = x.Satuan,
                            Qty_TT = x.Qty,
                            QtyBeli = x.Qty_Tlh_Dibeli,
                            Tlh_Diterima = x.QtyTerima,
                            Harga_Beli = x.Harga

                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string Detail(string id)
        {

            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.CSSD_GetHeaderPenerimaanLinen.FirstOrDefault(z => z.No_Penerimaan  == id );
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d = s.CSSD_GetDetailPenerimaanLinen.Where(x => x.No_Penerimaan == m.No_Penerimaan).OrderBy(x => x.Nama_Barang).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            Id = m.No_Penerimaan,
                            Tgl_Penerimaan = m.Tgl_Penerimaan.ToString("yyyy-MM-dd"),
                            Kode_Supplier = m.Kode_Supplier,
                            Nama_Supplier = m.Nama_Supplier,
                            No_Order = m.No_DO,
                            Nama_Lokasi = m.Nama_Lokasi,
                            NoDO = m.No_Order,
                            Tgl_JatuhTempo = m.Tgl_JatuhTempo.Value.ToString("yyyy-MM-dd"),
                            Remak = m.Remark,
                            GrandTotal = m.Total_Nilai
                        },
                        Detail = d.ConvertAll(x => new
                        {
                            Id = m.Penerimaan_ID,
                            Barang_ID = x.Barang_ID,
                            Kode_Barang = x.Kode_Barang,
                            Nama_Barang = x.Nama_Barang,
                            Satuan_Stok = x.Satuan_Stok,
                            Qty_TT = x.Qty_TT,
                            Tlh_Diterima = x.Tlh_Diterima,
                            Harga_Beli = x.Harga_Beli,
                            Diskon_1 = x.Diskon_1,
                            Diskon_Rp = x.Diskon_Rp
                            //Harga =  x.Harga
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P - Supplier

        [HttpPost]
        public string ListLookupSupplier(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.CSSD_GetSupplierSetupKontrak.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                                y.Kode_Supplier.Contains(x.Value) ||
                                y.Nama_Supplier.Contains(x.Value));
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        SupplierID = x.Supplier_ID,
                        Kode_Supplier = x.Kode_Supplier,
                        Nama_Supplier = x.Nama_Supplier,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

    }
}