﻿using CSSD.Helper;
using CSSD.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace CSSD.Controllers
{
    public class ReportController : Controller
    {
        private string serverpath = "~/Reports/";

        public ActionResult Index(string name, string category)
        {
            var hreport = new HReport(Server, serverpath);
            var r = new ReportViewModel();
            r.Name = name;
            r.Category = category;
            r.Reports = hreport.List(r.Category);
            return View(r);
        }

        public ActionResult ExportPDF(string category, string filename, string param)
        {
            try
            {
                var server = WebConfigurationManager.AppSettings["reportserver"];
                var database = WebConfigurationManager.AppSettings["reportdatabase"];
                var user = WebConfigurationManager.AppSettings["reportuser"];
                var pass = WebConfigurationManager.AppSettings["reportpass"];

                var hreport = new HReport(Server, serverpath);
                var parameters = JsonConvert.DeserializeObject<List<HReportParameter>>(param);
                var stream = hreport.ExportPDF(server, database, user, pass, category, filename, parameters);

                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                return File(stream, "application/pdf");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}