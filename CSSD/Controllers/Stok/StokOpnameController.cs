﻿using CSSD.Entities.SIM;
using CSSD.Helper;
using CSSD.Models.Stok;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace CSSD.Controllers.Stok
{
    
    public class StokOpnameController : Controller
    {
        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {   
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            { 
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.INV_ListStokOpname.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                            y.KelompokJenis.Contains(x.Value)

                            );
                        }
                    }
                    var tahun = int.Parse(filter.FirstOrDefault(x => x.Key == "Tahun").Value);
                    var bulan = int.Parse(filter.FirstOrDefault(x => x.Key == "Bulan").Value);
                    var _start = new DateTime(tahun, bulan, 1);
                    var _end = new DateTime(tahun, bulan, DateTime.DaysInMonth(tahun, bulan));
                    proses = proses.Where(y => y.Tgl_Opname >= _start && y.Tgl_Opname <= _end);

                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        No = m.No_Bukti,
                        Posted = m.Posted,
                        Tanggal = m.Tgl_Opname.ToString("dd/MM/yyyy"),
                        Kelompok = m.KelompokJenis,
                        Keterangan = m.Keterangan
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, StokOpnameViewModel model)
        {
            
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        string id = "";
                        if (_process == "CREATE")
                        {
                            if (model.Detail == null) model.Detail = new List<StokOpnameDetailViewModel>();
                            if (model.Detail.Count == 0) throw new Exception("Detail tidak boleh kosong");
                            id = s.INV_InsertStokOpname(
                                model.Tanggal,
                                model.Jenis,
                                model.Lokasi
                            ).FirstOrDefault();

                            if (string.IsNullOrEmpty(id)) throw new Exception("INV_InsertStokOpname tidak mendapatkan nobukti");

                            foreach (var x in model.Detail)
                            {
                                s.INV_InsertStokOpnameDetail(
                                    id,
                                    x.Id,
                                    model.Lokasi,
                                    x.Qty,
                                    x.Keterangan
                                );
                            }
                            s.SaveChanges();
                        }
                        else if (_process == "EDIT")
                        {
                            foreach (var x in model.Detail)
                            {
                                s.INV_UpdateStokOpnameDetail(
                                    model.NoBukti,
                                    x.Id,
                                    x.Qty,
                                    x.Keterangan
                                );
                            }
                            s.SaveChanges();
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"StokOpname-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new { IsSuccess = true, Id = id });
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
        }

        [HttpPost]
        public string Detail(string id)
        {
            
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.INV_ListStokOpnameHeader.FirstOrDefault(x => x.No_Bukti == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d = s.INV_ListStokOpnameDetail.Where(x => x.No_Bukti == m.No_Bukti).OrderBy(x => x.Kode_Barang).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            No = m.No_Bukti,
                            Tanggal = m.Tgl_Opname.ToString("yyyy-MM-dd"),
                            Lokasi = m.Lokasi_ID,
                            Posted = m.Posted,
                            Jenis = m.KelompokJenis
                        },
                        Detail = d.ConvertAll(x => new
                        {
                            Id = x.Barang_ID,
                            Kode = x.Kode_Barang,
                            Nama = x.Nama_Barang,
                            Satuan = x.NamaSatuan,
                            Konversi = x.Konversi,
                            QtySystem = x.Stock_Akhir,
                            QtyFisik = x.Qty_Opname,
                            Harga = x.Harga_Rata,
                            Kategori = x.Nama_Kategori,
                            Keterangan = x.Keterangan
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string ProsesSO(string id)
        {
            
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        s.INV_ProsesSO(id);
                        s.SaveChanges();

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"StokOpname-ProsesSO; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new { IsSuccess = true, Id = id });
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
        }

        #endregion

        #region ===== L O O K U P

        [HttpPost]
        public string ListLookupBarang(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                

                int totalcount;
                using (var s = new SIMEntities())
                {
                    var _lokasi = int.Parse(filter.FirstOrDefault(x => x.Key == "Lokasi").Value);
                    var _kelompok = filter.FirstOrDefault(x => x.Key == "Kelompok").Value;
                    var proses = s.INV_ListBarangOpname(_lokasi, _kelompok).AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y => y.Nama_Kategori.Contains(x.Value));
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.Barang_ID,
                        Kode = m.Kode_Barang,
                        Nama = m.Nama_Barang,
                        Satuan = m.Satuan,
                        Konversi = m.Konversi,
                        QtySystem = m.Qty_stok,
                        Kategori = m.Nama_Kategori,
                        Harga = m.Harga,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}