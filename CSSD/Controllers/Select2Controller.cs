﻿using CSSD.Entities.SIM;
using CSSD.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CSSD.Controllers
{
    public class Select2Controller : Controller
    {
        #region ===== Transaksi

        [HttpPost]
        public string Other_Linen_Dari(int pagesize, int pageindex, string filter)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var proses = s.CSSD_GetLinenDariLinenKotorInternal.AsQueryable();
                    if (!string.IsNullOrEmpty(filter))
                    {
                        proses = proses.Where(x =>
                            x.Kode_Lokasi.Contains(filter) ||
                            x.Nama_Lokasi.Contains(filter)
                        );
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy(x => x.Kode_Lokasi).Skip(pageindex * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.Lokasi_ID,
                        Kode = m.Kode_Lokasi,
                        Nama = m.Nama_Lokasi
                    });
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = r,
                        TotalCount = totalcount,
                        PageIndex = pageindex,
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }

        }
        //#region ===== J A S A

        //[HttpPost]
        //public string Jasa_Jasa_GroupJasa(int pagesize, int pageindex, string filter)
        //{
        //    try
        //    {
        //        using (var s = new SIMEntities())
        //        {
        //            var proses = s.ADM_ListGroupJasa.AsQueryable();
        //            if (!string.IsNullOrEmpty(filter))
        //            {
        //                proses = proses.Where(x =>
        //                    x.GroupJasaName.Contains(filter) ||
        //                    x.KelompokPerawatan.Contains(filter)
        //                );
        //            }
        //            var totalcount = proses.Count();
        //            var models = proses.OrderBy(x => x.GroupJasaName).Skip(pageindex * pagesize).Take(pagesize).ToList();
        //            var r = models.ConvertAll(m => new
        //            {
        //                Id = m.GroupJasaID,
        //                Nama = m.GroupJasaName
        //            });
        //            return JsonConvert.SerializeObject(new
        //            {
        //                IsSuccess = true,
        //                Data = r,
        //                TotalCount = totalcount,
        //                PageIndex = pageindex,
        //            });
        //        }
        //    }
        //    catch (SqlException ex) { return HConvert.Error(ex); }
        //    catch (Exception ex) { return HConvert.Error(ex); }

        //}

        //[HttpPost]
        //public string Jasa_Jasa_KategoriJasa(int pagesize, int pageindex, string filter, byte? groupjasa)
        //{
        //    try
        //    {
        //        using (var s = new SIMEntities())
        //        {
        //            var proses = s.ADM_GetKategoriJasa.AsQueryable();
        //            if (groupjasa != null) proses = proses.Where(x => x.GroupJasa == groupjasa);
        //            if (!string.IsNullOrEmpty(filter))
        //            {
        //                proses = proses.Where(x =>
        //                    x.KategoriJasaID.Contains(filter) ||
        //                    x.NamaInternational.Contains(filter) ||
        //                    x.KategoriJasaName.Contains(filter)
        //                );
        //            }
        //            var totalcount = proses.Count();
        //            var models = proses.OrderBy(x => x.GroupJasaName).Skip(pageindex * pagesize).Take(pagesize).ToList();
        //            var r = models.ConvertAll(m => new
        //            {
        //                Id = m.KategoriJasaID,
        //                Nama = m.KategoriJasaName
        //            });
        //            return JsonConvert.SerializeObject(new
        //            {
        //                IsSuccess = true,
        //                Data = r,
        //                TotalCount = totalcount,
        //                PageIndex = pageindex,
        //            });
        //        }
        //    }
        //    catch (SqlException ex) { return HConvert.Error(ex); }
        //    catch (Exception ex) { return HConvert.Error(ex); }

        //}

        //[HttpPost]
        //public string Jasa_Jasa_Dokter(int pagesize, int pageindex, string filter)
        //{
        //    try
        //    {
        //        using (var s = new SIMEntities())
        //        {
        //            var proses = s.ADM_ListVendor.AsQueryable();
        //            if (!string.IsNullOrEmpty(filter))
        //            {
        //                proses = proses.Where(x =>
        //                    x.Kode_Supplier.Contains(filter) ||
        //                    x.Nama_Supplier.Contains(filter)
        //                );
        //            }
        //            var totalcount = proses.Count();
        //            var models = proses.OrderBy(x => x.Kode_Supplier).Skip(pageindex * pagesize).Take(pagesize).ToList();
        //            var r = models.ConvertAll(m => new
        //            {
        //                Id = m.Kode_Supplier,
        //                Kode = m.Kode_Supplier,
        //                Nama = m.Nama_Supplier
        //            });
        //            return JsonConvert.SerializeObject(new
        //            {
        //                IsSuccess = true,
        //                Data = r,
        //                TotalCount = totalcount,
        //                PageIndex = pageindex,
        //            });
        //        }
        //    }
        //    catch (SqlException ex) { return HConvert.Error(ex); }
        //    catch (Exception ex) { return HConvert.Error(ex); }
        //}

        //[HttpPost]
        //public string Jasa_Jasa_Akun(int pagesize, int pageindex, string filter)
        //{
        //    try
        //    {
        //        using (var s = new SIMEntities())
        //        {
        //            var proses = s.ADM_GetKomponenAkun.AsQueryable();
        //            if (!string.IsNullOrEmpty(filter))
        //            {
        //                proses = proses.Where(x =>
        //                    x.Akun_No.Contains(filter) ||
        //                    x.Akun_Name.Contains(filter)
        //                );
        //            }
        //            var totalcount = proses.Count();
        //            var models = proses.OrderBy(x => x.Akun_No).Skip(pageindex * pagesize).Take(pagesize).ToList();
        //            var r = models.ConvertAll(m => new
        //            {
        //                Id = m.Akun_No,
        //                Kode = m.Akun_No,
        //                Nama = m.Akun_Name
        //            });
        //            return JsonConvert.SerializeObject(new
        //            {
        //                IsSuccess = true,
        //                Data = r,
        //                TotalCount = totalcount,
        //                PageIndex = pageindex,
        //            });
        //        }
        //    }
        //    catch (SqlException ex) { return HConvert.Error(ex); }
        //    catch (Exception ex) { return HConvert.Error(ex); }
        //}

        //[HttpPost]
        //public string Jasa_Jasa_Section(int pagesize, int pageindex, string filter)
        //{
        //    try
        //    {
        //        using (var s = new SIMEntities())
        //        {
        //            var proses = s.ADM_GetLookUpDetailSection.AsQueryable();
        //            if (!string.IsNullOrEmpty(filter))
        //            {
        //                proses = proses.Where(x =>
        //                    x.SectionName.Contains(filter) ||
        //                    x.ID.Contains(filter)
        //                );
        //            }
        //            var totalcount = proses.Count();
        //            var models = proses.OrderBy(x => x.SectionName).Skip(pageindex * pagesize).Take(pagesize).ToList();
        //            var r = models.ConvertAll(m => new
        //            {
        //                Id = m.ID,
        //                Nama = m.SectionName
        //            });
        //            return JsonConvert.SerializeObject(new
        //            {
        //                IsSuccess = true,
        //                Data = r,
        //                TotalCount = totalcount,
        //                PageIndex = pageindex,
        //            });
        //        }
        //    }
        //    catch (SqlException ex) { return HConvert.Error(ex); }
        //    catch (Exception ex) { return HConvert.Error(ex); }
        //}

        //[HttpPost]
        //public string Jasa_Jasa_DataLAB(int pagesize, int pageindex, string filter)
        //{
        //    try
        //    {
        //        using (var s = new SIMEntities())
        //        {
        //            var proses = s.ADM_GetLookUpDetailDataLAB.AsQueryable();
        //            if (!string.IsNullOrEmpty(filter))
        //            {
        //                proses = proses.Where(x =>
        //                    x.ID.Contains(filter) ||
        //                    x.NamaTest.Contains(filter)
        //                );
        //            }
        //            var totalcount = proses.Count();
        //            var models = proses.OrderBy(x => x.NamaTest).Skip(pageindex * pagesize).Take(pagesize).ToList();
        //            var r = models.ConvertAll(m => new
        //            {
        //                Id = m.ID,
        //                Nama = m.NamaTest
        //            });
        //            return JsonConvert.SerializeObject(new
        //            {
        //                IsSuccess = true,
        //                Data = r,
        //                TotalCount = totalcount,
        //                PageIndex = pageindex,
        //            });
        //        }
        //    }
        //    catch (SqlException ex) { return HConvert.Error(ex); }
        //    catch (Exception ex) { return HConvert.Error(ex); }
        //}
        //#endregion

        //#region ===== O T H E R

        //[HttpPost]
        //public string Other_Section_Customer(int pagesize, int pageindex, string filter)
        //{
        //    try
        //    {
        //        using (var s = new SIMEntities())
        //        {
        //            var proses = s.ADM_GetCustomerSection.AsQueryable();
        //            if (!string.IsNullOrEmpty(filter))
        //            {
        //                proses = proses.Where(x =>
        //                    x.Alamat_1.Contains(filter) ||
        //                    x.Nama_Customer.Contains(filter)
        //                );
        //            }
        //            var totalcount = proses.Count();
        //            var models = proses.OrderBy(x => x.Kode_Customer).Skip(pageindex * pagesize).Take(pagesize).ToList();
        //            var r = models.ConvertAll(m => new
        //            {
        //                Id = m.Customer_ID,
        //                Kode = m.Kode_Customer,
        //                Nama = m.Nama_Customer
        //            });
        //            return JsonConvert.SerializeObject(new
        //            {
        //                IsSuccess = true,
        //                Data = r,
        //                TotalCount = totalcount,
        //                PageIndex = pageindex,
        //            });
        //        }
        //    }
        //    catch (SqlException ex) { return HConvert.Error(ex); }
        //    catch (Exception ex) { return HConvert.Error(ex); }

        //}

        //[HttpPost]
        //public string Other_Section_AkunMutasi(int pagesize, int pageindex, string filter)
        //{
        //    try
        //    {
        //        using (var s = new SIMEntities())
        //        {
        //            var proses = s.ADM_GetRekKategoriVendor.AsQueryable();
        //            if (!string.IsNullOrEmpty(filter))
        //            {
        //                proses = proses.Where(x =>
        //                    x.Akun_No.Contains(filter) ||
        //                    x.Akun_Name.Contains(filter)
        //                );
        //            }
        //            var totalcount = proses.Count();
        //            var models = proses.OrderBy(x => x.Akun_No).Skip(pageindex * pagesize).Take(pagesize).ToList();
        //            var r = models.ConvertAll(m => new
        //            {
        //                Kode = m.Akun_No,
        //                Nama = m.Akun_Name
        //            });
        //            return JsonConvert.SerializeObject(new
        //            {
        //                IsSuccess = true,
        //                Data = r,
        //                TotalCount = totalcount,
        //                PageIndex = pageindex,
        //            });
        //        }
        //    }
        //    catch (SqlException ex) { return HConvert.Error(ex); }
        //    catch (Exception ex) { return HConvert.Error(ex); }

        //}



        //[HttpPost]
        //public string Other_PaketObat_Section(int pagesize, int pageindex, string filter)
        //{
        //    try
        //    {
        //        using (var s = new SIMEntities())
        //        {
        //            var proses = s.ADM_GetPaketObatSection.AsQueryable();
        //            if (!string.IsNullOrEmpty(filter))
        //            {
        //                proses = proses.Where(x =>
        //                    x.SectionID.Contains(filter) ||
        //                    x.SectionName.Contains(filter)
        //                );
        //            }
        //            var totalcount = proses.Count();
        //            var models = proses.OrderBy(x => x.SectionID).Skip(pageindex * pagesize).Take(pagesize).ToList();
        //            var r = models.ConvertAll(m => new
        //            {
        //                Id = m.SectionID,
        //                Kode = m.SectionID,
        //                Nama = m.SectionName
        //            });
        //            return JsonConvert.SerializeObject(new
        //            {
        //                IsSuccess = true,
        //                Data = r,
        //                TotalCount = totalcount,
        //                PageIndex = pageindex,
        //            });
        //        }
        //    }
        //    catch (SqlException ex) { return HConvert.Error(ex); }
        //    catch (Exception ex) { return HConvert.Error(ex); }

        //}

        //[HttpPost]
        //public string Other_JenisPembayaran_Rekening(int pagesize, int pageindex, string filter)
        //{
        //    try
        //    {
        //        using (var s = new SIMEntities())
        //        {
        //            var proses = s.ADM_GetRekKategoriVendor.AsQueryable();
        //            if (!string.IsNullOrEmpty(filter))
        //            {
        //                proses = proses.Where(x =>
        //                    x.Akun_No.Contains(filter) ||
        //                    x.Akun_Name.Contains(filter)
        //                );
        //            }
        //            var totalcount = proses.Count();
        //            var models = proses.OrderBy(x => x.Akun_No).Skip(pageindex * pagesize).Take(pagesize).ToList();
        //            var r = models.ConvertAll(m => new
        //            {
        //                Id = m.Akun_ID,
        //                Kode = m.Akun_No,
        //                Nama = m.Akun_Name
        //            });
        //            return JsonConvert.SerializeObject(new
        //            {
        //                IsSuccess = true,
        //                Data = r,
        //                TotalCount = totalcount,
        //                PageIndex = pageindex,
        //            });
        //        }
        //    }
        //    catch (SqlException ex) { return HConvert.Error(ex); }
        //    catch (Exception ex) { return HConvert.Error(ex); }

        //}

        //[HttpPost]
        //public string Other_Kamar_Sal(int pagesize, int pageindex, string filter)
        //{
        //    try
        //    {
        //        using (var s = new SIMEntities())
        //        {
        //            var proses = s.ADM_GetPaketObatSection.AsQueryable();
        //            if (!string.IsNullOrEmpty(filter))
        //            {
        //                proses = proses.Where(x =>
        //                    x.SectionID.Contains(filter) ||
        //                    x.SectionName.Contains(filter)
        //                );
        //            }
        //            var totalcount = proses.Count();
        //            var models = proses.OrderBy(x => x.SectionName).Skip(pageindex * pagesize).Take(pagesize).ToList();
        //            var r = models.ConvertAll(m => new
        //            {
        //                Id = m.SectionID,
        //                Nama = m.SectionName
        //            });
        //            return JsonConvert.SerializeObject(new
        //            {
        //                IsSuccess = true,
        //                Data = r,
        //                TotalCount = totalcount,
        //                PageIndex = pageindex,
        //            });
        //        }
        //    }
        //    catch (SqlException ex) { return HConvert.Error(ex); }
        //    catch (Exception ex) { return HConvert.Error(ex); }

        //}

        //[HttpPost]
        //public string Other_Kamar_Kelas(int pagesize, int pageindex, string filter)
        //{
        //    try
        //    {
        //        using (var s = new SIMEntities())
        //        {
        //            var proses = s.ADM_GetKelas.AsQueryable();
        //            if (!string.IsNullOrEmpty(filter))
        //            {
        //                proses = proses.Where(x =>
        //                    x.KelasID.Contains(filter) ||
        //                    x.NamaKelas.Contains(filter)
        //                );
        //            }
        //            var totalcount = proses.Count();
        //            var models = proses.OrderBy(x => x.NamaKelas).Skip(pageindex * pagesize).Take(pagesize).ToList();
        //            var r = models.ConvertAll(m => new
        //            {
        //                Id = m.KelasID,
        //                Nama = m.NamaKelas
        //            });
        //            return JsonConvert.SerializeObject(new
        //            {
        //                IsSuccess = true,
        //                Data = r,
        //                TotalCount = totalcount,
        //                PageIndex = pageindex,
        //            });
        //        }
        //    }
        //    catch (SqlException ex) { return HConvert.Error(ex); }
        //    catch (Exception ex) { return HConvert.Error(ex); }

        //}

        //[HttpPost]
        //public string Other_Diskon_Akun(int pagesize, int pageindex, string filter)
        //{
        //    try
        //    {
        //        using (var s = new SIMEntities())
        //        {
        //            var proses = s.ADM_GetNoAkunDiskon.AsQueryable();
        //            if (!string.IsNullOrEmpty(filter))
        //            {
        //                proses = proses.Where(x =>
        //                    x.Akun_Name.Contains(filter) ||
        //                    x.Akun_No.Contains(filter)
        //                );
        //            }
        //            var totalcount = proses.Count();
        //            var models = proses.OrderBy(x => x.Akun_No).Skip(pageindex * pagesize).Take(pagesize).ToList();
        //            var r = models.ConvertAll(m => new
        //            {
        //                Id = m.Akun_No,
        //                Kode = m.Akun_No,
        //                Nama = m.Akun_Name,
        //                Header = m.Induk
        //            });
        //            return JsonConvert.SerializeObject(new
        //            {
        //                IsSuccess = true,
        //                Data = r,
        //                TotalCount = totalcount,
        //                PageIndex = pageindex,
        //            });
        //        }
        //    }
        //    catch (SqlException ex) { return HConvert.Error(ex); }
        //    catch (Exception ex) { return HConvert.Error(ex); }
        //}

        //[HttpPost]
        //public string Other_Diskon_GroupJasa(int pagesize, int pageindex, string filter)
        //{
        //    try
        //    {
        //        using (var s = new SIMEntities())
        //        {
        //            var proses = s.ADM_DiskonGroupJasa.AsQueryable();
        //            if (!string.IsNullOrEmpty(filter))
        //            {
        //                proses = proses.Where(x =>
        //                    x.GroupJasaName.Contains(filter)
        //                );
        //            }
        //            var totalcount = proses.Count();
        //            var models = proses.OrderBy(x => x.GroupJasaName).Skip(pageindex * pagesize).Take(pagesize).ToList();
        //            var r = models.ConvertAll(m => new
        //            {
        //                Id = m.GroupJasaID,
        //                Nama = m.GroupJasaName
        //            });
        //            return JsonConvert.SerializeObject(new
        //            {
        //                IsSuccess = true,
        //                Data = r,
        //                TotalCount = totalcount,
        //                PageIndex = pageindex,
        //            });
        //        }
        //    }
        //    catch (SqlException ex) { return HConvert.Error(ex); }
        //    catch (Exception ex) { return HConvert.Error(ex); }
        //}

        //[HttpPost]
        //public string Other_Diskon_Komponen(int pagesize, int pageindex, string filter)
        //{
        //    try
        //    {
        //        using (var s = new SIMEntities())
        //        {
        //            var proses = s.ADM_DiskonKomponen.AsQueryable();
        //            if (!string.IsNullOrEmpty(filter))
        //            {
        //                proses = proses.Where(x =>
        //                    x.KomponenBiayaID.Contains(filter) ||
        //                    x.KomponenName.Contains(filter)
        //                );
        //            }
        //            var totalcount = proses.Count();
        //            var models = proses.OrderBy(x => x.KomponenName).Skip(pageindex * pagesize).Take(pagesize).ToList();
        //            var r = models.ConvertAll(m => new
        //            {
        //                Id = m.KomponenBiayaID,
        //                Nama = m.KomponenName
        //            });
        //            return JsonConvert.SerializeObject(new
        //            {
        //                IsSuccess = true,
        //                Data = r,
        //                TotalCount = totalcount,
        //                PageIndex = pageindex,
        //            });
        //        }
        //    }
        //    catch (SqlException ex) { return HConvert.Error(ex); }
        //    catch (Exception ex) { return HConvert.Error(ex); }
        //}


        //[HttpPost]
        //public string Other_AwalSystem_Akun(int pagesize, int pageindex, string filter)
        //{
        //    try
        //    {
        //        using (var s = new SIMEntities())
        //        {
        //            var proses = s.ADM_GetAkunSetupAwal.AsQueryable();
        //            if (!string.IsNullOrEmpty(filter))
        //            {
        //                proses = proses.Where(x =>
        //                    x.Akun_No.Contains(filter) ||
        //                    x.Akun_Name.Contains(filter)
        //                );
        //            }
        //            var totalcount = proses.Count();
        //            var models = proses.OrderBy(x => x.Akun_No).Skip(pageindex * pagesize).Take(pagesize).ToList();
        //            var r = models.ConvertAll(m => new
        //            {
        //                Id = m.AKun_ID,
        //                Kode = m.Akun_No,
        //                Nama = m.Akun_Name
        //            });
        //            return JsonConvert.SerializeObject(new
        //            {
        //                IsSuccess = true,
        //                Data = r,
        //                TotalCount = totalcount,
        //                PageIndex = pageindex,
        //            });
        //        }
        //    }
        //    catch (SqlException ex) { return HConvert.Error(ex); }
        //    catch (Exception ex) { return HConvert.Error(ex); }

        //}

        //#endregion

        //#region ===== K A W I

        //[HttpPost]
        //public string Jasa_ItemKomponenData_AkunHPP(int pagesize, int pageindex, string filter)
        //{
        //    try
        //    {
        //        using (var s = new SIMEntities())
        //        {
        //            var proses = s.ADM_AkunHPP.AsQueryable();
        //            if (!string.IsNullOrEmpty(filter))
        //            {
        //                proses = proses.Where(x =>
        //                    x.Akun_No.Contains(filter) ||
        //                    x.Akun_Name.Contains(filter)
        //                );
        //            }
        //            var totalcount = proses.Count();
        //            var models = proses.OrderBy(x => x.Akun_No).Skip(pageindex * pagesize).Take(pagesize).ToList();
        //            var r = models.ConvertAll(m => new
        //            {

        //                Id = m.Akun_No,
        //                Name = m.Akun_Name,
        //                Header = m.Header

        //            });
        //            return JsonConvert.SerializeObject(new
        //            {
        //                IsSuccess = true,
        //                Data = r,
        //                TotalCount = totalcount,
        //                PageIndex = pageindex,
        //            });
        //        }
        //    }
        //    catch (SqlException ex) { return HConvert.Error(ex); }
        //    catch (Exception ex) { return HConvert.Error(ex); }

        //}

        //[HttpPost]
        //public string Jasa_ItemKomponenData_AkunLawanHPP(int pagesize, int pageindex, string filter)
        //{
        //    try
        //    {
        //        using (var s = new SIMEntities())
        //        {
        //            var proses = s.ADM_AkunLawanHPP.AsQueryable();
        //            if (!string.IsNullOrEmpty(filter))
        //            {
        //                proses = proses.Where(x =>
        //                    x.Akun_Name.Contains(filter) ||
        //                    x.Akun_No.Contains(filter)
        //                );
        //            }
        //            var totalcount = proses.Count();
        //            var models = proses.OrderBy(x => x.Akun_No).Skip(pageindex * pagesize).Take(pagesize).ToList();
        //            var r = models.ConvertAll(m => new
        //            {

        //                Id = m.Akun_No,
        //                Name = m.Akun_Name,
        //                Header = m.Header

        //            });
        //            return JsonConvert.SerializeObject(new
        //            {
        //                IsSuccess = true,
        //                Data = r,
        //                TotalCount = totalcount,
        //                PageIndex = pageindex,
        //            });
        //        }
        //    }
        //    catch (SqlException ex) { return HConvert.Error(ex); }
        //    catch (Exception ex) { return HConvert.Error(ex); }

        //}

        //public string Jasa_ADM_AkunGroupJasa(int pagesize, int pageindex, string filter)
        //{
        //    try
        //    {
        //        using (var s = new SIMEntities())
        //        {
        //            var proses = s.ADM_AkunGroupJasa.AsQueryable();
        //            if (!string.IsNullOrEmpty(filter))
        //            {
        //                proses = proses.Where(x =>
        //                    x.Akun_Name.Contains(filter) ||
        //                    x.Akun_No.Contains(filter)
        //                );
        //            }
        //            var totalcount = proses.Count();
        //            var models = proses.OrderBy(x => x.Akun_No).Skip(pageindex * pagesize).Take(pagesize).ToList();
        //            var r = models.ConvertAll(m => new
        //            {

        //                Id = m.Akun_No,
        //                Name = m.Akun_Name,
        //                Header = m.Header

        //            });
        //            return JsonConvert.SerializeObject(new
        //            {
        //                IsSuccess = true,
        //                Data = r,
        //                TotalCount = totalcount,
        //                PageIndex = pageindex,
        //            });
        //        }
        //    }
        //    catch (SqlException ex) { return HConvert.Error(ex); }
        //    catch (Exception ex) { return HConvert.Error(ex); }

        //}

        //public string Jasa_ADM_ListGroupJasa(int pagesize, int pageindex, string filter)
        //{
        //    try
        //    {
        //        using (var s = new SIMEntities())
        //        {
        //            var proses = s.ADM_ListGroupJasa.AsQueryable();
        //            if (!string.IsNullOrEmpty(filter))
        //            {
        //                proses = proses.Where(x =>
        //                    x.GroupJasaID.ToString().Contains(filter) ||
        //                    x.GroupJasaName.Contains(filter)
        //                );
        //            }
        //            var totalcount = proses.Count();
        //            var models = proses.OrderBy(x => x.GroupJasaID).Skip(pageindex * pagesize).Take(pagesize).ToList();
        //            var r = models.ConvertAll(m => new
        //            {

        //                Id = m.GroupJasaID,
        //                Name = m.GroupJasaName,
        //                CSSDDokter = m.CSSDDokter,
        //                INcentive = m.INcentive,
        //                THT = m.THT
        //            });
        //            return JsonConvert.SerializeObject(new
        //            {
        //                IsSuccess = true,
        //                Data = r,
        //                TotalCount = totalcount,
        //                PageIndex = pageindex,
        //            });
        //        }
        //    }
        //    catch (SqlException ex) { return HConvert.Error(ex); }
        //    catch (Exception ex) { return HConvert.Error(ex); }

        //}

        //public string Jasa_ADM_GetKategoriPlafonJasa(int pagesize, int pageindex, string filter)
        //{
        //    try
        //    {
        //        using (var s = new SIMEntities())
        //        {
        //            var proses = s.ADM_GetKategoriPlafonJasa.AsQueryable();
        //            if (!string.IsNullOrEmpty(filter))
        //            {
        //                proses = proses.Where(x =>
        //                    x.KategoriPlafon.ToString().Contains(filter) ||
        //                    x.Tipe.Contains(filter)
        //                );
        //            }
        //            var totalcount = proses.Count();
        //            var models = proses.OrderBy(x => x.KategoriPlafon).Skip(pageindex * pagesize).Take(pagesize).ToList();
        //            var r = models.ConvertAll(m => new
        //            {

        //                Id = m.KategoriPlafon,
        //                Name = m.Tipe,

        //            });
        //            return JsonConvert.SerializeObject(new
        //            {
        //                IsSuccess = true,
        //                Data = r,
        //                TotalCount = totalcount,
        //                PageIndex = pageindex,
        //            });
        //        }
        //    }
        //    catch (SqlException ex) { return HConvert.Error(ex); }
        //    catch (Exception ex) { return HConvert.Error(ex); }

        //}

        //public string Jasa_ADM_ListJasa(int pagesize, int pageindex, string filter)
        //{
        //    try
        //    {
        //        using (var s = new SIMEntities())
        //        {
        //            var proses = s.ADM_ListJasa.AsQueryable();
        //            if (!string.IsNullOrEmpty(filter))
        //            {
        //                proses = proses.Where(x =>
        //                    x.JasaID.ToString().Contains(filter) ||
        //                    x.JasaName.Contains(filter)
        //                );
        //            }
        //            var totalcount = proses.Count();
        //            var models = proses.OrderBy(x => x.JasaID).Skip(pageindex * pagesize).Take(pagesize).ToList();
        //            var r = models.ConvertAll(m => new
        //            {
        //                Id = m.JasaID,
        //                Name = m.JasaName,
        //            });
        //            return JsonConvert.SerializeObject(new
        //            {
        //                IsSuccess = true,
        //                Data = r,
        //                TotalCount = totalcount,
        //                PageIndex = pageindex,
        //            });
        //        }
        //    }
        //    catch (SqlException ex) { return HConvert.Error(ex); }
        //    catch (Exception ex) { return HConvert.Error(ex); }

        //}
        //public string Jasa_ADM_GetTipeCSSDJasaTidakCSSD(int pagesize, int pageindex, string filter)
        //{
        //    try
        //    {
        //        using (var s = new SIMEntities())
        //        {
        //            var proses = s.ADM_GetTipeCSSDJasaTidakCSSD.AsQueryable();
        //            if (!string.IsNullOrEmpty(filter))
        //            {
        //                proses = proses.Where(x =>
        //                    x.TipeCSSD.ToString().Contains(filter)
        //                );
        //            }
        //            var totalcount = proses.Count();
        //            var models = proses.OrderBy(x => x.TipeCSSD).Skip(pageindex * pagesize).Take(pagesize).ToList();
        //            var r = models.ConvertAll(m => new
        //            {
        //                Id = m.TipeCSSD,
        //                Name = m.TipeCSSD,

        //            });
        //            return JsonConvert.SerializeObject(new
        //            {
        //                IsSuccess = true,
        //                Data = r,
        //                TotalCount = totalcount,
        //                PageIndex = pageindex,
        //            });
        //        }
        //    }
        //    catch (SqlException ex) { return HConvert.Error(ex); }
        //    catch (Exception ex) { return HConvert.Error(ex); }

        //}
        //public string Jasa_ADM_GetKomisiObatTipePasienr(int pagesize, int pageindex, string filter)
        //{
        //    try
        //    {
        //        using (var s = new SIMEntities())
        //        {
        //            var proses = s.ADM_GetKomisiObatTipePasien.AsQueryable();
        //            if (!string.IsNullOrEmpty(filter))
        //            {
        //                proses = proses.Where(x =>
        //                   x.JenisKerjasama.Contains(filter) ||
        //                   x.JenisKerjasamaID.ToString().Contains(filter)
        //                );
        //            }
        //            var totalcount = proses.Count();
        //            var models = proses.OrderBy(x => x.JenisKerjasamaID).Skip(pageindex * pagesize).Take(pagesize).ToList();
        //            var r = models.ConvertAll(m => new
        //            {
        //                Id = m.JenisKerjasamaID,
        //                Name = m.JenisKerjasama
        //            });
        //            return JsonConvert.SerializeObject(new
        //            {
        //                IsSuccess = true,
        //                Data = r,
        //                TotalCount = totalcount,
        //                PageIndex = pageindex,
        //            });
        //        }
        //    }
        //    catch (SqlException ex) { return HConvert.Error(ex); }
        //    catch (Exception ex) { return HConvert.Error(ex); }

        //}

        //public string Jasa_ADM_GetSectionICD(int pagesize, int pageindex, string filter)
        //{
        //    try
        //    {
        //        using (var s = new SIMEntities())
        //        {
        //            var proses = s.ADM_GetSectionICD.AsQueryable();
        //            if (!string.IsNullOrEmpty(filter))
        //            {
        //                proses = proses.Where(x =>
        //                   x.SectionID.Contains(filter) ||
        //                   x.SectionName.ToString().Contains(filter)
        //                );
        //            }
        //            var totalcount = proses.Count();
        //            var models = proses.OrderBy(x => x.SectionID).Skip(pageindex * pagesize).Take(pagesize).ToList();
        //            var r = models.ConvertAll(m => new
        //            {
        //                Id = m.SectionID,
        //                Name = m.SectionName
        //            });
        //            return JsonConvert.SerializeObject(new
        //            {
        //                IsSuccess = true,
        //                Data = r,
        //                TotalCount = totalcount,
        //                PageIndex = pageindex,
        //            });
        //        }
        //    }
        //    catch (SqlException ex) { return HConvert.Error(ex); }
        //    catch (Exception ex) { return HConvert.Error(ex); }

        //}
        //public string Jasa_ADM_GetDiagnosaHCICD(int pagesize, int pageindex, string filter)
        //{
        //    try
        //    {
        //        using (var s = new SIMEntities())
        //        {
        //            var proses = s.ADM_GetDiagnosaHCICD.AsQueryable();
        //            if (!string.IsNullOrEmpty(filter))
        //            {
        //                proses = proses.Where(x =>
        //                   x.DiagnosaHC.Contains(filter) ||
        //                   x.NoUrut.ToString().Contains(filter)
        //                );
        //            }
        //            var totalcount = proses.Count();
        //            var models = proses.OrderBy(x => x.DiagnosaHC).Skip(pageindex * pagesize).Take(pagesize).ToList();
        //            var r = models.ConvertAll(m => new
        //            {
        //                Id = m.DiagnosaHC,
        //                Name = m.DiagnosaHC
        //            });
        //            return JsonConvert.SerializeObject(new
        //            {
        //                IsSuccess = true,
        //                Data = r,
        //                TotalCount = totalcount,
        //                PageIndex = pageindex,
        //            });
        //        }
        //    }
        //    catch (SqlException ex) { return HConvert.Error(ex); }
        //    catch (Exception ex) { return HConvert.Error(ex); }

        //}

        #endregion
    }
}