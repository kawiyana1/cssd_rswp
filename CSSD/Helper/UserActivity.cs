﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using CSSD.Entities.Activity;
using CSSD.Entities.Identity;

namespace CSSD.Helper
{
    public class UserActivity
    {
        public static void InsertUserActivity(UserActivityModel model)
        {
            try
            {
                string username;
                using (var s = new IdentityEntities()) 
                {
                    var m = s.AspNetUsers.FirstOrDefault(x => x.Id == model.Id_User);
                    username = m == null ? "Anonymous" : m.UserName;
                }
                using (var s = new ActivityEntities())
                {
                    var r = s.aCSSD.Add(new aCSSD()
                    {
                        Id_User = model.Id_User,
                        InputDate = model.InputDate,
                        IP = model.IP,
                        OS = model.OS,
                        UserFullName = username,
                        Browser = model.Browser,
                        Activity = model.Activity
                    });
                    s.SaveChanges();
                }
            }
            catch (DbEntityValidationException ex){ throw new Exception("User Activity : " + ex.ExToString()); }
            catch (SqlException ex) { throw new Exception("User Activity : " + ex.Message); }
            catch (Exception ex) { throw new Exception("User Activity : " + ex.Message); }
        }
    }

    public class UserActivityModel
    {
        public UserActivityModel(HttpRequestBase RequestBase, string Id_User)
        {
            this.RequestBase = RequestBase;
            this.Id_User = Id_User;
            this.Id_Cabang = Id_Cabang;
            this.Id_UnitBisnis = Id_UnitBisnis;
        }

        public HttpRequestBase RequestBase { get; set; }
        public string Id_User { get; set; }
        public int? Id_Cabang { get; set; }
        public int? Id_UnitBisnis { get; set; }

        public string IP { get { return RequestBase.UserHostName; } }
        public DateTime InputDate { get { return DateTime.Now; } }
        public string OS
        {
            get
            {
                var x = RequestBase.UserAgent;
                var x1 = x.IndexOf('(') + 1;
                var x2 = x.IndexOf(')') - x1;
                var userAgent = x.Substring(x1, x2);
                return userAgent;
            }
        }
        public string Browser { get { return RequestBase.Browser.Browser + " " + RequestBase.Browser.Version; } }
        public string Activity { get; set; }
    }
}