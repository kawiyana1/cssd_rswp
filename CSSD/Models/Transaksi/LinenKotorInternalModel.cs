﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CSSD.Models.Transaksi
{
    public class LinenKotorInternalModel
        {
            public string No_Bukti { get; set; }
            public DateTime? Tgl_Mutasi { get; set; }
            public short? Lokasi_Asal { get; set; }
            public string Keterangan { get; set; }

            public List<LinenKotorInternalModelDetailModel> Detail { get; set; }
        }

    public class LinenKotorInternalModelDetailModel
    {
        
        public int? Barang_ID { get; set; }
        public string Kode_Satuan { get; set; }
        public double? Qty { get; set; }
        public decimal? Harga { get; set; }

    }
    
}