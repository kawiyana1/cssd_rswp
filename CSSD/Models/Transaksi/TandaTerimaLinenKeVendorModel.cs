﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CSSD.Models.Transaksi
{
    public class TandaTerimaLinenKeVendorModel
    {
        public string NoTandaTerima { get; set; }
        public string No_TT { get; set; }
        public string Kode_Supplier { get; set; }
        public DateTime? Tanggal { get; set; }
        public string Keterangan { get; set; }

        public List<TandaTerimaLinenKeVendorDetailModel> Detail { get; set; }
    }

    public class TandaTerimaLinenKeVendorDetailModel
    {

        public int? Barang_ID { get; set; }
        public string Kode_Item { get; set; }
        public string Kode_Satuan_Beli { get; set; }
        public string JenisJasa { get; set; }
        public short? Qty { get; set; }
        public string Keterangan { get; set; }
    }
}