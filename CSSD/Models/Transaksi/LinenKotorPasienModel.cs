﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CSSD.Models.Transaksi
{
        public class LinenKotorPasienModel
        {
            public string No_Bukti { get; set; }
            public DateTime Tanggal { get; set; }
            public int Jml_Item { get; set; }
            public decimal Nilai_Transaksi { get; set; }
            public decimal Total_Nilai { get; set; }
            public decimal Nilai_DP { get; set; }
            public string Keterangan { get; set; }
            public string No_SuratJalan { get; set; }
            public decimal Add_Service { get; set; }
            public string TypeTransaksi { get; set; }
            public string NRM { get; set; }
            public string NamaPasien { get; set; }
            public string SectionName { get; set; }
            public string NoReg { get; set; }
            public DateTime? Tanggal_Selesai { get; set; }
            public DateTime? Waktu_Selesai { get; set; }
            public DateTime? Waktu_Transaksi { get; set; }
            public string Alasan_Batal { get; set; }

            public List<LinenKotorPasienModelDetailModel> Detail { get; set; }
        }

    public class LinenKotorPasienModelDetailModel
    {
        
        public double? Diskon { get; set; }
        public decimal? Harga_Satuan { get; set; }
        public string Keterangan { get; set; }
        public string Kode_Item { get; set; }
        public string No_Bukti { get; set; }
        public string Alasan_Batal { get; set; }
        public short? NOUrut { get; set; }
        public double? Qty { get; set; }

    }
    
}